import { defineStore } from "pinia";
import WsRequest from "@/api/WsRequest";
import Message from "@/mode/Message";
import Receipt from "@/mode/Receipt";

export const useWsStore = defineStore({
  id: "ws_store",
  state: () => ({
    wsRequest: WsRequest.getInstance(),
  }),

  actions: {
    init(): void {
      this.wsRequest.init();
    },
    close(): void {
      this.wsRequest.close();
    },
    send(str: string): void {
      this.wsRequest.send(str);
    },
    sendMessage(message: Message): void {
      this.wsRequest.sendMessage(message);
    },
    sendRead(receipt: Receipt): void {
      this.wsRequest.sendRead(receipt);
    },
    checkStatus(): void {
      this.wsRequest.checkStatus();
    },
  },
});
